# Guide d'installation de l'application

Ce guide fournit des instructions pour installer et exécuter l'application. Veuillez suivre les étapes ci-dessous pour configurer l'environnement et démarrer l'application avec succès.

## Choix d'utilisation NestJS

L'application est développée en utilisant le framework NestJS. NestJS est un framework de développement d'applications serveur en Node.js qui permet de créer des applications robustes et évolutives en utilisant des concepts inspirés d'Angular. 

Comme vous êtes déjà familier avec Angular, vous trouverez qu'il est plus facile de comprendre les mécanismes de NestJS. C'est également une excellente occasion de découvrir NestJS si vous ne l'avez pas encore utilisé ☺️.

### Architecture de l'application

L'architecture de l'application suit le modèle en couches de NestJS. Voici quelques points clés à noter :

- Le point d'entrée de l'application est le fichier main.ts, qui charge le module principal ``` (appModule) ```.
- Les contrôleurs (controllers), décorés par ``` @Controller ```, gèrent les routes et les requêtes d'entrée, ainsi que les réponses correspondantes.
- Les services (services), décorés par ``` @Injectable ```, permettent de partager des données entre les différents composants de l'application et contiennent du code réutilisable pour la logique métier.
- Les modules ``` (modules) ``` regroupent des composants, définissent leurs dépendances et leur visibilité. Ils facilitent l'organisation et la structure du code de l'application.

Pour plus d'informations sur l'architecture et les concepts de NestJS, veuillez consulter la documentation officielle sur https://docs.nestjs.com/.

### Structure de projet

![](docs/structure.png)

### Prérequis

Avant de commencer, assurez-vous de disposer des éléments suivants :

- Node.js version 16+ installé sur votre système.
- Un gestionnaire de paquets Node.js tel que npm ou yarn.

### Installation de l'application

Installez les dépendances en exécutant la commande suivante.

``` npm install ``` ou  ``` yarn install ```

L'installation des paquets devrait prendre quelques minutes la première fois, puis elle est mise en cache et sera plus rapide.

### Démarrage de l'application

Pour démarrer l'application, exécutez la commande suivante dans le répertoire de l'application :

- Environnement de production :

``` npm run start ``` ou ``` yarn run start ```

- Ou environnement de développement :

``` npm run start:dev ``` ou ``` yarn run start:dev ```

L'application sera lancée et écoutera les requêtes sur le port 3000 par défaut.

`localhost:3000`

### Exécution des tests

``` npm run test ``` ou ``` npm run test:watch ```

### Utilisation de l'API

L'application expose une route POST `/mouvements/validation` qui permet de soumettre des mouvements et des soldes. L'API s'attend à recevoir le corps (body) de la requête suivant le format JSON

```
{
  "balances": [
    { "date": "2000-01-12T00:00:00.000Z", "balance": 0.0 },
    { "date": "2023-01-12T00:00:00.000Z", "balance": 500.0 },
    { "date": "2023-02-14T00:00:00.000Z", "balance": 400.0 },
    { "date": "2023-03-12T00:00:00.000Z", "balance": 350.0 },
    { "date": "2023-04-12T00:00:00.000Z", "balance": 425.0 }
  ],
  "mouvements": [
    {
      "id": 1,
      "date": "2023-01-01T00:00:00.000Z",
      "label": "Dépense 1",
      "amount": 500.0
    },
    {
      "id": 2,
      "date": "2023-01-13T00:00:00.000Z",
      "label": "Dépense 1",
      "amount": -100.0
    },
    {
      "id": 3,
      "date": "2023-02-15T00:00:00.000Z",
      "label": "Dépense 2",
      "amount": -50.0
    },
    {
      "id": 4,
      "date": "2023-03-16T00:00:00.000Z",
      "label": "Dépense 3",
      "amount": 75.0
    }
  ]
}

```

Vous trouverez dans le fichier `data.ts` situé dans le répertoire `src/fixtures` le jeu de données.

---