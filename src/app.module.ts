import { Module } from '@nestjs/common';
import { MouvementsModule } from './modules/mouvements/mouvements.module';
@Module({
  imports: [MouvementsModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
