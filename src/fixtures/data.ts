export const CHECKPOINTS = [
  { date: new Date(2000, 0, 12), balance: 0.0 },
  { date: new Date(2023, 0, 12), balance: 500.0 },
  { date: new Date(2023, 1, 14), balance: 400.0 },
  { date: new Date(2023, 2, 12), balance: 350.0 },
  { date: new Date(2023, 3, 12), balance: 425.0 },
];

const DEFAULT_OPERATIONS = [
  {
    id: 1,
    date: new Date(2023, 0, 1),
    label: 'Dépense 1',
    amount: 500.0,
  },
  {
    id: 1,
    date: new Date(2023, 0, 13),
    label: 'Dépense 1',
    amount: -100.0,
  },
  {
    id: 2,
    date: new Date(2023, 1, 15),
    label: 'Dépense 2',
    amount: -50.0,
  },
  {
    id: 3,
    date: new Date(2023, 2, 16),
    label: 'Dépense 3',
    amount: 75.0,
  },
];

export const OPERATIONS_ACCEPTED = [...DEFAULT_OPERATIONS];
export const OPERATIONS_DUPPLICATED = [
  ...DEFAULT_OPERATIONS,
  {
    id: 1,
    date: new Date(2023, 0, 1),
    label: 'Dépense 1',
    amount: 500.0,
  },
];

export const OPERATIONS_MISSING_DATE = [
  {
    id: 10,
    label: 'Dépense 1',
    amount: 500.0,
  },
];

export const CHECKPOINTS_MISMATCH = [
  { date: new Date(2000, 0, 12), balance: 0.0 },
  { date: new Date(2023, 0, 12), balance: 500.0 },
  { date: new Date(2023, 1, 14), balance: 300.0 }, // <-- mismatch
  { date: new Date(2023, 2, 12), balance: 350.0 },
  { date: new Date(2023, 3, 12), balance: 425.0 },
];
