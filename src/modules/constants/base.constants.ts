export const MISSING_DATE = 'Movement missing for date';
export const DUPLICATE_MOVEMENT = 'Duplicate movement';
export const MISMATCH_MOVEMENT = 'Balance mismatch for movement';
export const TEAPOT_MESSAGE = "I'm a teapot";
export const ACCEPTED_MESSAGE = 'Accepted';
export const STATUS_CODE_ACCEPTED = 202;
export const STATUS_CODE_TEAPOT = 418;
