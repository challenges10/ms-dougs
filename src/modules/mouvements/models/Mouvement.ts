interface Operation {
  id: number;
  date?: Date;
  label: string;
  amount: number;
}

interface Checkpoint {
  date: Date;
  balance: number;
}

interface Reason {
  date: Date;
  reason: string;
}

interface ValidationResult {
  status: number;
  message: string;
  reasons?: Reason[];
}
