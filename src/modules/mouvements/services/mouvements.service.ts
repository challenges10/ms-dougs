import { Injectable } from '@nestjs/common';
import {
  DUPLICATE_MOVEMENT,
  MISMATCH_MOVEMENT,
  MISSING_DATE,
  STATUS_CODE_ACCEPTED,
  STATUS_CODE_TEAPOT,
  ACCEPTED_MESSAGE,
  TEAPOT_MESSAGE,
} from 'src/modules/constants';

@Injectable()
export class MouvementssService {
  validateBankOperations(
    operations: Operation[],
    checkpoints: Checkpoint[],
  ): ValidationResult {
    const reasons: Reason[] = [];

    for (let i = 1; i < checkpoints.length; i++) {
      const currentCheckpoint = checkpoints[i];
      const previousCheckpoint = checkpoints[i - 1];

      // Filter operations between previous and current checkpoint dates
      const filteredOperations = operations.filter(
        (op) =>
          op.date < currentCheckpoint.date && op.date > previousCheckpoint.date,
      );

      // Calculate the sum of amounts for filtered operations
      const sum = filteredOperations.reduce(
        (total, op) => total + op.amount,
        0,
      );

      // Calculate the current balance based on previous checkpoint and sum of amounts
      const currentBalance = previousCheckpoint.balance + sum;

      // Check if the current balance matches the balance in the current checkpoint
      if (currentBalance !== currentCheckpoint.balance) {
        let reason: string;
        if (filteredOperations.length === 0) {
          reason = MISSING_DATE;
        } else if (filteredOperations.length > 1) {
          reason = DUPLICATE_MOVEMENT;
        } else {
          reason = MISMATCH_MOVEMENT;
        }
        reasons.push({
          date: currentCheckpoint.date,
          reason,
        });
      }
    }

    if (reasons.length > 0) {
      return {
        status: STATUS_CODE_TEAPOT,
        message: TEAPOT_MESSAGE,
        reasons,
      };
    }

    return {
      status: STATUS_CODE_ACCEPTED,
      message: ACCEPTED_MESSAGE,
    };
  }
}
