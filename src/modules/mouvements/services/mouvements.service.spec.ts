import { Test, TestingModule } from '@nestjs/testing';
import { MouvementssService } from './mouvements.service';
import {
  CHECKPOINTS,
  CHECKPOINTS_MISMATCH,
  OPERATIONS_ACCEPTED,
  OPERATIONS_DUPPLICATED,
  OPERATIONS_MISSING_DATE,
} from 'src/fixtures';
import {
  DUPLICATE_MOVEMENT,
  MISMATCH_MOVEMENT,
  MISSING_DATE,
  STATUS_CODE_ACCEPTED,
  STATUS_CODE_TEAPOT,
  ACCEPTED_MESSAGE,
  TEAPOT_MESSAGE,
} from 'src/modules/constants';

describe('MouvementssService', () => {
  let mouvementssService: MouvementssService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MouvementssService],
    }).compile();

    mouvementssService = module.get<MouvementssService>(MouvementssService);
  });

  it('MouvementssService - should be defined', () => {
    expect(mouvementssService).toBeDefined();
  });

  it('should return Accepted when operations are valid', () => {
    const result: ValidationResult = mouvementssService.validateBankOperations(
      OPERATIONS_ACCEPTED,
      CHECKPOINTS,
    );
    expect(result.status).toBe(STATUS_CODE_ACCEPTED);
    expect(result.message).toBe(ACCEPTED_MESSAGE);
    expect(result.reasons).toBeUndefined();
  });
  it("should return I'm a teapot when operations are duplicated", () => {
    const result: ValidationResult = mouvementssService.validateBankOperations(
      OPERATIONS_DUPPLICATED,
      CHECKPOINTS,
    );
    expect(result.status).toBe(STATUS_CODE_TEAPOT);
    expect(result.message).toBe(TEAPOT_MESSAGE);
    expect(result.reasons).toBeDefined();
    expect(result.reasons[0].reason).toBe(DUPLICATE_MOVEMENT);
  });
  it("should return I'm a teapot when operations are missing date", () => {
    const result: ValidationResult = mouvementssService.validateBankOperations(
      OPERATIONS_MISSING_DATE,
      CHECKPOINTS,
    );
    expect(result.status).toBe(STATUS_CODE_TEAPOT);
    expect(result.message).toBe(TEAPOT_MESSAGE);
    expect(result.reasons).toBeDefined();
    expect(result.reasons[0].reason).toBe(MISSING_DATE);
  });

  it("should return I'm a teapot when operations are mismatched", () => {
    const result: ValidationResult = mouvementssService.validateBankOperations(
      OPERATIONS_ACCEPTED,
      CHECKPOINTS_MISMATCH,
    );
    expect(result.status).toBe(STATUS_CODE_TEAPOT);
    expect(result.message).toBe(TEAPOT_MESSAGE);
    expect(result.reasons).toBeDefined();
    expect(result.reasons[0].reason).toBe(MISMATCH_MOVEMENT);
  });
});
