import { Test } from '@nestjs/testing';
import { MouvementssService } from '../services/mouvements.service';
import { MouvementsController } from './mouvements.controller';
import { CHECKPOINTS, OPERATIONS_ACCEPTED } from 'src/fixtures';
import {
  STATUS_CODE_ACCEPTED,
  ACCEPTED_MESSAGE,
} from 'src/modules/constants';

describe('MouvementsController', () => {
  let mouvementsController: MouvementsController;
  let mouvementsService: MouvementssService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [MouvementsController],
      providers: [MouvementssService],
    }).compile();

    mouvementsController =
      moduleRef.get<MouvementsController>(MouvementsController);
    mouvementsService = moduleRef.get<MouvementssService>(MouvementssService);
  });

  describe('validation', () => {
    it('should call validateBankOperations in mouvementsService and return the result', () => {
      const expectedResult = {
        status: STATUS_CODE_ACCEPTED,
        message: ACCEPTED_MESSAGE,
      };

      jest
        .spyOn(mouvementsService, 'validateBankOperations')
        .mockReturnValue(expectedResult);
      const result = mouvementsController.validation({
        mouvements: OPERATIONS_ACCEPTED,
        balances: CHECKPOINTS,
      });
      expect(result).toBe(expectedResult);
    });
  });
});
