import { Body, Controller, Post } from '@nestjs/common';
import { MouvementssService } from '../services/mouvements.service';
import { MouvementsDto } from '../objects/Mouvements.dto';

@Controller('mouvements')
export class MouvementsController {
  constructor(private readonly mouvementsService: MouvementssService) {}

  @Post('/validation')
  validation(@Body() dto: MouvementsDto): ValidationResult {
    const { mouvements, balances } = dto;
    return this.mouvementsService.validateBankOperations(mouvements, balances);
  }
}
