import { Module } from '@nestjs/common';
import { MouvementsController } from './controllers/mouvements.controller';
import { MouvementssService } from './services/mouvements.service';

@Module({
  imports: [],
  providers: [MouvementssService],
  exports: [],
  controllers: [MouvementsController],
})
export class MouvementsModule {}
