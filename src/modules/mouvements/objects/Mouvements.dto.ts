import { IsArray } from 'class-validator';

export class MouvementsDto {
  @IsArray()
  mouvements: Array<Operation>;
  @IsArray()
  balances: Array<Checkpoint>;
}
